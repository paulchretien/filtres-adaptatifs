# Particles Filter Plane Simulator

## Influence de l'environement et des différents paramètres


Le premier environnement comporte des rectangles plus ou moins hauts et larges.
Voici le comportement observé :

![env1_params1](/home/paul.chretien/Documents/5A/TP_Fiad/filtres-adaptatifs/particle_filter_student/img/env1_params_1_1.png "First environnement and first parameters")

![env1_params1](/home/paul.chretien/Documents/5A/TP_Fiad/filtres-adaptatifs/particle_filter_student/img/env1_params_1_2.png "First environnement and first parameters")

 